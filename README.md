# Paladin Aura API

A Roll20 API that auto-applies Paladin Auras and shows which characters are affected by the aura through the use of token-markers.

Depreciated version: https://github.com/LaytonGB/PaladinAura